export interface User {
    id?:number;
    email:string;
    password?:string;
    pseudo: string;
    role?:string;
}


export interface Comment{
    id?:number;
    content:string;
    createAt:string;
    autor:User;
}

export interface Picture{
    id?:number;
    image:string;
    description:string;
    title:string;
    autor:User;
}