import type { Picture } from "@/entities";
import axios from "axios";



export async function fetchAllPictures() {
    const response = await axios.get<Picture[]>('http://localhost:8080/api/picture');
    return response.data;
}

export async function fetchOnePicture(id: any) {
    const response = await axios.get<Picture>("http://localhost:8080/api/picture/" + id);
    return response.data;
}

export async function fetchAllPictureByCategorie(id: any) {
    const response = await axios.get<Picture[]>("http://localhost:8080/api/picture/sort/" + id);
    return response.data;
}

export async function createPicture(picture: Picture) {
    const response = await axios.post<Picture>("http://localhost:8080/api/picture", picture);
    return response.data;
}

export async function updatePicture(picture: Picture) {
    const response = await axios.put<Picture>("http://localhost:8080/api/picture/" + picture.id, picture);
    return response.data;
}

export async function deletePicture(id: any) {
    const response = await axios.delete<Picture>("http://localhost:8080/api/picture/" + id);
    return response.data;
}


export async function addLike(id: any) {
    const response = await axios.put<Picture>("http://localhost:8080/api/picture/" + id + "/like");
    return response.data;
}

export async function addDisLike(id: any) {
    const response = await axios.put<Picture>("http://localhost:8080/api/picture/" + id + "/dislike");
    return response.data;
}